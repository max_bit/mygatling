import json
import random
import string
import copy

launcher = ['mock', 'simulated', 'dfadfdaf']
fetcher = ['mock', 'git', 'dfadfas']

def random_string(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def valid_file_gen(num_files, for_pools=False):
    base_file = open("lock.json", "r")
    file = json.load(base_file)
    if for_pools:
        file['name'] = random_string()
        file['minimum'] = 2
        file['maximum'] = 4
        del file['lifetime']
    for i in range(num_files):
        temp = copy.deepcopy(file)
        temp['data']['test'] = random_string()
        with open(f"valid_lock_{i}.json", "w") as f:
            json.dump(temp, f, indent=2, separators=(',', ': '))


def invalid_file_gen(num_files, for_pools=False):
    base_file = open("lock.json", "r")
    file = json.load(base_file)
    if for_pools:
        file['name'] = random_string()
        file['minimum'] = 2
        file['maximum'] = 4
        del file['lifetime']
    for i in range(num_files):
        temp = copy.deepcopy(file)
        fetcher_name = random.choice(fetcher)
        launcher_name = random.choice(launcher)
        if fetcher_name == "mock":
            rng = random.random()
            if rng > 0.5:
                temp['data']['fetcher_fetch_error'] = "True"
        elif fetcher_name == "git":
            rng = random.random()
            if rng > 0.5:
                temp['meta'] = ''
            else:
                temp['meta'][random_string()] = random_string()

        temp['launcher'] = launcher_name
        temp['fetcher'] = fetcher_name
        with open(f"invalid_lock_{i}.json", "w") as f:
            json.dump(temp, f, indent=2, separators=(',', ': '))


if __name__ == "__main__":
    valid_file_gen(5, for_pools=False)
    invalid_file_gen(15)
