package org.mcalderoni.mygatling

import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * https://blog.codecentric.de/en/2017/06/gatling-load-testing-part-1-using-gatling/
 */
class MySimulation extends Simulation {

  private val httpConfig = http.baseUrl("http://computer-database.gatling.io")

  private val scn = scenario("MySimulation").
    exec(http("open").
      get("/")).
    pause(1)

  setUp(scn.inject(atOnceUsers(1))).protocols(httpConfig)

}
