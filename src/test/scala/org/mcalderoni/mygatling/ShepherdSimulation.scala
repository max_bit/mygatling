package org.mcalderoni.mygatling

import com.typesafe.scalalogging.LazyLogging
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

object ShepherdSimulation extends LazyLogging {

//  val urlString = "http://10.148.253.59:5000"
  val urlString = "http://127.0.0.1"

  val httpConfig = http.baseUrl(s"$urlString:5000")

  val feeder = Array(
    Map("lockRequest" -> "src/test/resources/download/valid_lock_0.json"),
//    Map("lockRequest" -> "src/test/resources/download/valid_lock_1.json"),
//    Map("lockRequest" -> "src/test/resources/download/valid_lock_2.json"),
//    Map("lockRequest" -> "src/test/resources/download/valid_lock_3.json"),
//    Map("lockRequest" -> "src/test/resources/download/valid_lock_4.json"),
//    Map("lockRequest" -> "src/test/resources/download/invalid_lock_0.json"),
//    Map("lockRequest" -> "src/test/resources/download/invalid_lock_1.json"),
    Map("lockRequest" -> "src/test/resources/download/invalid_lock_2.json")
  ).random

  lazy val headers = Map(
    "accept-language" -> "en-US,en;q=0.9,it;q=0.8",
    "content-type" -> "application/json",
    "accept" -> "application/json, text/plain, */*",
  )

  private val uuid = http("lock").
    get("/lock/uuid").headers(headers)
    .check(status.is(200))
    .check(bodyString.saveAs("lockId"))

  private val requestLock = http("lock").
    post("/lock/${lockId}").headers(headers)
    .body(RawFileBody("${lockRequest}")).asJson

  val createLock = exec(uuid).exec(requestLock)

  val scn = scenario("ShepherdSimulation")
    .feed(feeder)
//    .exec(createLock).pause(1)
    .repeat(1)(createLock)

}

/**
  * https://blog.codecentric.de/en/2017/06/gatling-load-testing-part-1-using-gatling/
  */
class ShepherdSimulation extends Simulation {
  import ShepherdSimulation._

  setUp(scn

    .inject(atOnceUsers(2))

//    .inject(constantUsersPerSec(20).during(10 seconds))
    .protocols(httpConfig)
//    .throttle(
//      reachRps(8) in (5),
//      holdFor(10 seconds)
//    )
  )
}
